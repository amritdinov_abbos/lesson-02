package main

import (
	"fmt"
)

func main() {
	fmt.Println(MySquareRoot(10, 12))
}

func MySquareRoot(num, precision uint) (result float64) {
	var i uint
	for i <= num {
		if i*i == num {
			return float64(i)
		}
		i++
	}
}
