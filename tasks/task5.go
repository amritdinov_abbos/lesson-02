package main

import "fmt"

func main() {
	n := 3
	fmt.Scanf("%v", &n)
	// Read n from input
	// DisplayMinimumNumberFunction(n)
	fmt.Println(DisplayMinimumNumberFunction(n))
}

// https://www.hackerrank.com/contests/w30/challenges/find-the-minimum-number
func DisplayMinimumNumberFunction(n int) string {
	if n <= 2 {
		return "min(int,int)"
	}
	return "min(int," + DisplayMinimumNumberFunction(n-1) + ")"

}
